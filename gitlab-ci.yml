include:
  - 'https://gitlab.com/Linaro/lkft/pipelines/common/-/raw/20210728/common.yml'

variables:
  tuxconfig: "https://gitlab.com/Linaro/lkft/users/alex.bennee/linux-pipeline/-/raw/master/tuxconfig.yml"
  default_lava_tests: "--test-case ltp-syscalls.yaml"
  QA_TEAM: '~alex.bennee'

  RUN_TEST: "1"

# This will only build and test branches specified here.
workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
    - if: '$CI_COMMIT_BRANCH =~ /^review.*/'
      when: always
    - when: always

# No need to change
stages:
  - download_prerequisites
  - prerequisites
  - build
  - sanity
  - test
  - report
  - review
  - upstream

# Change so it suites you
#########################

# Builds
build-arm64-gcc-9:
  extends:
    - .build
  variables:
    build_set_name: arm64-gcc-9

build-arm64-gcc-9-kasan:
  extends:
    - .build
  variables:
    build_set_name: arm64-gcc-9-kasan

build-arm64-gcc-9-kunit:
  extends:
    - .build
  variables:
    build_set_name: arm64-gcc-9-kunit

build-arm64-gcc-9-64k_page_size:
  extends:
    - .build
  variables:
    build_set_name: arm64-gcc-9-64k_page_size

build-arm-gcc-9:
  extends:
    - .build
  variables:
    build_set_name: arm-gcc-9

build-arm-gcc-9-kasan:
  extends:
    - .build
  variables:
    build_set_name: arm-gcc-9-kasan

build-arm-gcc-9-kunit:
  extends:
    - .build
  variables:
    build_set_name: arm-gcc-9-kunit

build-x86-gcc-9:
  extends:
    - .build
  variables:
    build_set_name: x86-gcc-9

build-x86-gcc-9-kasan:
  extends:
    - .build
  variables:
    build_set_name: x86-gcc-9-kasan

build-x86-gcc-9-kunit:
  extends:
    - .build
  variables:
    build_set_name: x86-gcc-9-kunit

build-i386-gcc-9:
  extends:
    - .build
  variables:
    build_set_name: i386-gcc-9

build-i386-gcc-9-kunit:
  extends:
    - .build
  variables:
    build_set_name: i386-gcc-9-kunit

# Tests
test-juno-64k_page_size:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: juno-r2
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm64-gcc-9-64k_page_size
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm64-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64-kasan:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "${default_lava_tests}"
    QA_ENVIRONMENT: "qemu-arm64-kasan"
  needs:
    - build-arm64-gcc-9-kasan
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64-kunit:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "--test-case kunit.yaml"
  needs:
    - build-arm64-gcc-9-kunit
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64-compat:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "--test-case ltp-syscalls.yaml"
    ROOTFS_MACHINE: "am57xx-evm"
    QA_ENVIRONMENT: "qemu_arm64-compat"
  needs:
    - build-arm64-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-arm:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-arm-kasan:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-arm-kasan"
  needs:
    - build-arm-gcc-9-kasan
    - job: download_prerequisites
      artifacts: true

test-qemu-arm-kunit:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "--test-case kunit.yaml"
  needs:
    - build-arm-gcc-9-kunit
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-x86-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64-kasan:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "${default_lava_tests}"
    QA_ENVIRONMENT: "qemu-x86_64-kasan"
  needs:
    - build-x86-gcc-9-kasan
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64-kunit:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "--test-case kunit.yaml"
  needs:
    - build-x86-gcc-9-kunit
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64-compat:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "--test-case ltp-syscalls.yaml"
    ROOTFS_MACHINE: "intel-core2-32"
    QA_ENVIRONMENT: "qemu_x86_64-compat"
  needs:
    - build-x86-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-i386:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-i386-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-i386-kunit:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
    LAVA_TESTS: "--test-case kunit.yaml"
  needs:
    - build-i386-gcc-9-kunit
    - job: download_prerequisites
      artifacts: true
