include:
  - 'https://gitlab.com/Linaro/lkft/pipelines/common/-/raw/20201104/common.yml'

variables:
  tuxconfig: "https://gitlab.com/Linaro/lkft/users/anders.roxell/linux-pipeline/raw/master/sample/tuxconfig.yml"

workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /.*/'
      when: always

# No need to change
stages:
  - prerequisites
  - build
  - sanity
  - test

# Build stage jobs
build-x86-gcc-9:
  extends:
    - .build
  variables:
    build_set_name: x86-gcc-9

build-i386-gcc-9:
  extends:
    - .build
  variables:
    build_set_name: i386-gcc-9

build-arm64-gcc-9:
  extends:
    - .build
  variables:
    build_set_name: arm64-gcc-9

build-arm-gcc-9:
  extends:
    - .build
  variables:
    build_set_name: arm-gcc-9

buildset-arc:
  extends:
    - .build
  variables:
    build_set_name: arc

buildset-arm:
  extends:
    - .build
  variables:
    build_set_name: arm

buildset-arm64:
  extends:
    - .build
  variables:
    build_set_name: arm64

buildset-i386:
  extends:
    - .build
  variables:
    build_set_name: i386

buildset-mips:
  extends:
    - .build
  variables:
    build_set_name: mips

buildset-riscv:
  extends:
    - .build
  variables:
    build_set_name: riscv
  rules:
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.9.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.14.y"'
      when: never
    - when: always

buildset-x86:
  extends:
    - .build
  variables:
    build_set_name: x86

# Sanity stage jobs
sanity-qemu-arm:
  extends:
    - .sanity
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
  needs:
    - build-arm-gcc-9
    - job: download_prerequisites
      artifacts: true

sanity-qemu-arm64:
  extends:
    - .sanity
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
  needs:
    - build-arm64-gcc-9
    - job: download_prerequisites
      artifacts: true

sanity-qemu-x86_64:
  extends:
    - .sanity
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
  needs:
    - build-x86-gcc-9
    - job: download_prerequisites
      artifacts: true

sanity-qemu-i386:
  extends:
    - .sanity
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
  needs:
    - build-i386-gcc-9
    - job: download_prerequisites
      artifacts: true

# Test stage jobs
test-hikey:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: hi6220-hikey
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm64-gcc-9
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never

test-qemu-arm64:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm64-gcc-9
    - job: download_prerequisites
      artifacts: true

test-juno:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: juno-r2
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm64-gcc-9
    - job: download_prerequisites
      artifacts: true

test-juno-compat:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: juno-r2
    LAVA_TESTS: "--test-case ltp-syscalls.yaml"
    ROOTFS_MACHINE: "am57xx-evm"
    QA_ENVIRONMENT: "juno-r2-compat"
  needs:
    - build-arm64-gcc-9
    - job: download_prerequisites
      artifacts: true

test-x15:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: x15
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-arm:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm-gcc-9
    - job: download_prerequisites
      artifacts: true

test-x86_64:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: x86
    LAVA_TESTS: "${default_lava_tests}"
    #explicit:
    #ROOTFS_MACHINE: "intel-corei7-64"
  needs:
    - build-x86-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-x86-gcc-9
    - job: download_prerequisites
      artifacts: true

test-i386:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: i386
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-i386-gcc-9
    - job: download_prerequisites
      artifacts: true

test-qemu-i386:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-i386-gcc-9
    - job: download_prerequisites
      artifacts: true
